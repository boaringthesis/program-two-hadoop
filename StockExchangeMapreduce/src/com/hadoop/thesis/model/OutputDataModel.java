package com.hadoop.thesis.model;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class OutputDataModel implements Writable {

    public Text date;
    public DoubleWritable value;

    public OutputDataModel() {
        date = new Text();
        value = new DoubleWritable();
    }

    public OutputDataModel(Text date, DoubleWritable value) {
        this.date = date;
        this.value = value;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        date.write(dataOutput);
        value.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        date.readFields(dataInput);
        value.readFields(dataInput);
    }

    @Override
    public String toString(){
        String output = date.toString() + " " + value.toString();
        return output;
    }
}
