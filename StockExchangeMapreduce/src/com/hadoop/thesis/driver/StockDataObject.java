package com.hadoop.thesis.driver;

import com.hadoop.thesis.mapper.StockDataMapper;
import com.hadoop.thesis.model.OutputDataModel;
import com.hadoop.thesis.model.StockExchangeModel;
import com.hadoop.thesis.reducer.StockDataReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class StockDataObject extends Configured implements Tool {

    public static void main(String[] args) throws Exception{
        Timer time = new Timer();
        time.start();
        int res = ToolRunner.run(new Configuration(), new StockDataObject(), args);
        time.stop();
        System.out.println(time.showElapsedTime());
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();

        //Job.getInstance provides the config and the job name
        Job job = Job.getInstance(conf, "Stock Exchange Data Analysis");

        // set jar by finding where the given class name came from
        job.setJarByClass(StockDataObject.class);

        //set the mapper, reducer, and the combiner
        //with the above classes we defined
        job.setMapperClass(StockDataMapper.class);

        //job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(StockDataReducer.class);

        //set the data type for the mapper's key
        job.setMapOutputKeyClass(Text.class);

        //set the data type for the mapper's value
        job.setMapOutputValueClass(StockExchangeModel.class);

        //set the data type for the key in the output of reducer
        job.setOutputKeyClass(Text.class);

        //set the data type for the value in the output of reducer
        job.setOutputValueClass(OutputDataModel.class);

        //provide the location of the input and output directory
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
