package com.hadoop.thesis.test;

import com.hadoop.thesis.model.StockExchangeModel;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.Iterator;

public class StockDataReducer extends MapReduceBase implements Reducer<Text, StockExchangeModel, Text, Text> {
    @Override
    public void reduce(Text key, Iterator<StockExchangeModel> iterator, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
        double minValue = Double.MAX_VALUE;
        String date = "";

        while (iterator.hasNext()) {
            StockExchangeModel ow = iterator.next();
            if (ow.getValue().get() < minValue) {
                minValue = ow.getValue().get();
                date = ow.getDate().toString();
            }
        }

        outputCollector.collect(key, new Text("Date: " + date + ", Min-Value: " + minValue));
    }
}
