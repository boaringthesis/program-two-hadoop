package com.hadoop.thesis.test;

import com.hadoop.thesis.model.StockExchangeModel;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

public class StockDataMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, StockExchangeModel> {
    private String mapTaskId;
    private String inputFile;

    @Override
    public void map(LongWritable longWritable, Text text, OutputCollector<Text, StockExchangeModel> outputCollector, Reporter reporter) {
        String line = text.toString();
        String[] values = line.split(",");
//        double lowValue = 0;

        try {
            Text date = new Text(values[0]);
            DoubleWritable low = new DoubleWritable(Double.parseDouble(values[values.length - 2]));
            StockExchangeModel model = new StockExchangeModel(date, low);
//            model.setDate(date);
//            model.setValue(low);
//            ObjectWritable ow = new ObjectWritable();
//            ow.set(model);
            outputCollector.collect(new Text(inputFile), model);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void configure(JobConf job) {
        mapTaskId = job.get(JobContext.TASK_ATTEMPT_ID);
        inputFile = job.get(JobContext.MAP_INPUT_FILE);

    }
}
