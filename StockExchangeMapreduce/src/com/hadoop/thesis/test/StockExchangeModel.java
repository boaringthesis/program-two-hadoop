package com.hadoop.thesis.test;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class StockExchangeModel implements Writable {
    private Text date;
    private DoubleWritable value;

    public StockExchangeModel() {
    }

    public StockExchangeModel(Text date, DoubleWritable value) {
        this.date = date;
        this.value = value;
    }

    public Text getDate() {
        return date;
    }

    public void setDate(Text date) {
        this.date = date;
    }

    public DoubleWritable getValue() {
        return value;
    }

    public void setValue(DoubleWritable value) {
        this.value = value;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        date.write(dataOutput);
        value.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        date.readFields(dataInput);
        value.readFields(dataInput);
    }
}
