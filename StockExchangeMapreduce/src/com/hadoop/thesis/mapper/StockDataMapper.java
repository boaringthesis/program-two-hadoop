package com.hadoop.thesis.mapper;

import com.hadoop.thesis.model.StockExchangeModel;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class StockDataMapper extends Mapper<LongWritable, Text, Text, StockExchangeModel> {

    @Override
    public void map(LongWritable key, Text value, Context context) { // if try...catch... not used, then use: throws IOException, InterruptedException
        String line = value.toString();
        String[] values = line.split(",");

        try {
            Text date = new Text(values[1]);
            DoubleWritable low = new DoubleWritable(Double.parseDouble(values[values.length - 2]));
            StockExchangeModel model = new StockExchangeModel(date, low);
//            FileSplit fileSplit = (FileSplit)context.getInputSplit();
//            String file = fileSplit.getPath().getName();

            context.write(new Text(values[0]), model);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
