package com.hadoop.thesis.reducer;

import com.hadoop.thesis.model.OutputDataModel;
import com.hadoop.thesis.model.StockExchangeModel;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class StockDataReducer extends Reducer<Text, StockExchangeModel, Text, OutputDataModel> {
    @Override
    public void reduce(Text key, Iterable<StockExchangeModel> values, Context context) throws IOException, InterruptedException {
        double minValue = Double.MAX_VALUE;
        Text date = new Text();
        DoubleWritable value = new DoubleWritable();

        for (StockExchangeModel sem : values) {
            if (minValue > sem.value.get()) {
                date = sem.date;
                value = sem.value;
            }
        }

        OutputDataModel odm = new OutputDataModel(date, value);
        context.write(key, odm);
    }
}
